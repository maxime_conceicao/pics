﻿
## Déploiement d'application sur Heroku

https://medium.com/@JohanPujol/int%C3%A9gration-continue-d%C3%A9ploiement-continue-ci-cd-avec-gitlab-et-heroku-b809801a1524

### Initialisation du git flow sur pics

Passage sur la branche develop

### Création des applications sur Heroku

- heroku-pics 
- staging-heroku-pics

### Mise en place du CI

- création du .gitlab-ci.yml

### Mise en place du CD

Déclaration des variables HEROKU_STATING_API_KEY et HEROKU_PRODUCTION_API_KEY avec la clé API de Heroku
Mise à jour du .gitlab-ci.yml en passant la clé API de la pré prod

Après le push, l'application est disponible à l'adresse : https://staging-heroku-pics.herokuapp.com/

### Mise en place sur la production

merge de develop sur master
Mise à jour du .gitlab-ci.yml en passant la clé API de la production

Après le push, l'application est disponible à l'adresse : https://heroku-pics.herokuapp.com/


